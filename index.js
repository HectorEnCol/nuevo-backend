'use strict'

var mongoose = require('mongoose');
var app = require('./app');
var port = 3800;
var bd = 'cursomeansocial';

//-- Proceso de conexión con la Base de Datos --
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/' + bd, { useMongoClient: true })
        .then(() => {
            console.log("La conexión a la Base de Datos " + bd + " se ha realizado correctamente");

            //-- Creación del servidor --
            app.listen(port, () => {
                console.log("Servidor corriendo en http://localhost:" + port);
            });
        })
        .catch(err => console.log(err));